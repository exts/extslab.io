module.exports = {
  locales: {
    '/': {
      lang: 'ru-RU',
      title: 'MARKETPLACE',
      description: 'Расширения, дополнения, стили для CMS и CMF.'
    },
    '/en/': {
      lang: 'en-US',
      title: 'MARKETPLACE',
      description: 'Extensions, add-ons, styles for CMS and CMF.'
    }
  },
  base: '/',
  dest: 'public',
  head: [
    ['link', {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&subset=cyrillic'
    }],
    ['link', {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&subset=cyrillic'
    }],
    ['link', {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i&subset=cyrillic'
    }],
    ['link', {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Fira+Code:400,700&subset=cyrillic'
    }],
    ['link', {rel: 'stylesheet', href: 'https://cdn-storage.github.io/styles/vuepress/theme.min.css'}],
    ['link', {rel: 'icon', href: `/logo.png`}],
    ['link', {rel: 'manifest', href: '/manifest.json'}],
    ['meta', {name: 'theme-color', content: '#f3f6f9'}],
    ['meta', {name: 'apple-mobile-web-app-capable', content: 'yes'}],
    ['meta', {name: 'apple-mobile-web-app-status-bar-style', content: 'black'}],
    ['link', {rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png`}],
    ['link', {rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#f3f6f9'}],
    ['meta', {name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png'}],
    ['meta', {name: 'msapplication-TileColor', content: '#000000'}]
  ],
  markdown: {
    lineNumbers: true
  },
  plugins: [
    ['@vuepress/active-header-links', true],
    ['@vuepress/back-to-top', true],
    ['@vuepress/medium-zoom', true],
    ['@vuepress/nprogress', true],
    ['@vuepress/pwa', {
      serviceWorker: true,
      updatePopup: true
    }]
  ],
  themeConfig: {
    editLinks: false,
    locales: {
      '/': {
        label: 'Русский',
        selectText: 'RU',
        serviceWorker: {
          updatePopup: {
            message: "Доступна новая версия контента.",
            buttonText: "Обновить"
          }
        },
        nav: require('./nav/ru'),
        sidebar: {
          '/': [
            {
              title: 'Drupal',
              collapsable: false,
              children: [
                '/drupal/modules/',
                '/drupal/themes/',
              ]
            },
            {
              title: 'Flarum',
              collapsable: false,
              children: [
                '/flarum/extensions/',
                '/flarum/languages/',
                '/flarum/themes/',
              ]
            },
            {
              title: 'MediaWiki',
              collapsable: false,
              children: [
                '/mediawiki/extensions/',
                '/mediawiki/skins/',
              ]
            },
            {
              title: 'WordPress',
              collapsable: false,
              children: [
                '/wordpress/plugins/',
                '/wordpress/themes/',
              ]
            },
            {
              title: 'XenForo',
              collapsable: false,
              children: [
                '/xenforo/extensions/',
                '/xenforo/languages/',
                '/xenforo/styles/',
              ]
            }
          ]
        }
      },
      '/en/': {
        label: 'English',
        selectText: 'EN',
        nav: require('./nav/en'),
        sidebar: {
          '/en/': [
            {
              title: 'CMS / CMF',
              collapsable: false,
              children: []
            }
          ]
        }
      }
    }
  }
};